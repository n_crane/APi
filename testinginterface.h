#ifndef TESTINGINTERFACE_H
#define TESTINGINTERFACE_H

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/text.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/core.hpp"

#include <vector>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <fstream>
#include <math.h>


using namespace std;
using namespace cv;


class TestingInterface
{
public:

    enum ERROR_ID {

        NOSYMBOL, WRONGTEXT

    };
    enum SENSIBILITY {

        WEAK, MEDIUM, STRONG
    };
    // Region of interest
        struct m_ROI {
            int x = 0;
            int y = 0;
            int h = 0;
            int w = 0;
        };

        bool readinImages(std::vector<string>& images, cv::ImreadModes Imagemode);
        bool checkImages();
        virtual bool check(SENSIBILITY Sense);
        virtual bool check(SENSIBILITY Sense, double threshold);
        virtual bool check(SENSIBILITY Sense, double threshold, m_ROI roi);
        virtual bool check(m_ROI *regOI, double th,double* angle, SENSIBILITY sense);

protected:
        string m_image_ref;
        string m_image_taken;
        Mat m_Ref;
        Mat m_Taken;


};

#endif // TESTINGINTERFACE_H
