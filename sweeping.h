#ifndef SWEEPING_H
#define SWEEPING_H

#include "testinginterface.h"

class Sweeping : public TestingInterface
{

private:
    // Sweep attributes

        Mat m_Sweeparray_ref[60];
        Mat m_Sweeparray_taken[60];
        double m_angles[60];
        // Calculate Angle between 3 points
            double getAngleABC(int ax, int ay, int bx, int by, int cx, int cy);
public:
    virtual ~Sweeping();

            //TODO: readin array of images for reference or taken.
            bool readinSweep(const string image, bool takref);
            bool getlocationSweep(const string image);
            // TODO (check if correct): Acquisition:Calculate angle between tip of needle, source of needle and lot to source of needle.
            // Save results in array for later validation.
            bool readinangle(string tipneedle, m_ROI needle_source);
            // TODO (check if correct): Validation: Calculate angle and compare to Reference-angles.
            virtual bool check(m_ROI *regOI, double th,double* angle, SENSIBILITY sense);
};

#endif // SWEEPING_H
