#ifndef TEST_API_H
#define TEST_API_H

#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/text.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/core.hpp"

#include <vector>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <fstream>
#include <math.h>

/**
    Project: TESTAPI

    @author Niclas Crane
    @version 1.1 17/08/17
*/
// TO COMPILE: make TestApi
// EXECUTE: ./TestAPI

using namespace std;
using namespace cv;
//using namespace cv::text;
using namespace cv::xfeatures2d;

enum ERROR_ID {

    NOSYMBOL, WRONGTEXT

};

enum SENSIBILITY {

    WEAK, MEDIUM, STRONG
};
// Region of interest
    struct m_ROI {
        int x = 0;
        int y = 0;
        int h = 0;
        int w = 0;
    };

class TestValidation {

private:
// Imagenames
    string m_image_ref;
    string m_image_taken;
// ImageMats
    Mat m_Ref;
    Mat m_Taken;
// checks
    Mat m_check_Ref;
    Mat m_check_Taken;
// Comparevalues
    double m_histcompvalue[6];

    Rect m_Roibase;
//	 Threshold
    double m_th;
    Mat Thresh;
// symbols
    int m_loc_x, m_loc_y, m_w, m_h;
// Sweep attributes

    Mat m_Sweeparray_ref[60];
    Mat m_Sweeparray_taken[60];
    double m_angles[60];
// Calculate check
    bool calculatecheck(string image);
// Calculate Angle between 3 points
    double getAngleABC(int ax, int ay, int bx, int by, int cx, int cy);

public:

    // TODO: Read in all symbols in one picture. Use findcontours() to seperate all symbols and save in seperate imagefile.

    // Read in images for Reference or Taken or both
    bool readinImages(std::vector<string>& images, cv::ImreadModes Imagemode);
    //TODO: read in all needed symbols out of one image
    bool readinSymbols(const string image);
    //TODO: readin array of images for reference or taken.
    bool readinSweep(const string image, bool takref);
    //TODO: Get location of moving area. and maybe source.
    bool getlocationSweep(const string image);
    // TODO:Check if all infos are the same of reference and taken.
    bool checkImages();
    // Compare checks of ref and taken.
    bool check(SENSIBILITY sense);
    // Compare checks of ref and taken in specifique region.
    bool check(m_ROI *regOI, SENSIBILITY sense);
    // Compare checks of ref and taken in specifique region and apply threshold before.
    bool check(m_ROI *regOI, double th, SENSIBILITY sense);
    // Find symbol in taken picture (here it is m_ref).
    bool symbol(string symbol_wanted, double th, SENSIBILITY sense);
    // TODO (check if correct): Acquisition:Calculate angle between tip of needle, source of needle and lot to source of needle.
    // Save results in array for later validation.
    bool sweepangle(string tipneedle, m_ROI needle_source);
    // TODO (check if correct): Validation: Calculate angle and compare to Reference-angles.
    bool sweepangle(string tipneedle, double *angle, m_ROI needle_source);
};

#endif
