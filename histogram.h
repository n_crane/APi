#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include "testinginterface.h"

class Histogram : public TestingInterface{

private:

// Histograms
Mat m_Histogram_Ref;
Mat m_Histogram_Taken;
// Comparevalues
    double m_histcompvalue[6];
    Rect m_Roibase;
//	 Threshold
    double m_th;
    Mat Thresh;
    // Calculate Histogram
        bool calculatehistogram(string image);
public:
    virtual ~Histogram();
        // Compare Histograms of ref and taken.
    virtual    bool check(SENSIBILITY sense);
        // Compare Histograms of ref and taken in specifique region.
     virtual   bool check(m_ROI *regOI, SENSIBILITY sense);
        // Compare Histograms of ref and taken in specifique region and apply threshold before.
       virtual bool check(m_ROI *regOI, double th, SENSIBILITY sense);

};

#endif // HISTOGRAM_H
