#include "testinginterface.h"



bool TestingInterface::readinImages(std::vector<std::__cxx11::string> &images, cv::ImreadModes Imagemode)
{
    bool stat = false;

    if (images.size() > 1) {
        //Read in images by parameter.
        m_image_ref = images[0];
        m_image_taken = images[1];
        m_Ref = imread(m_image_ref, Imagemode);
        m_Taken = imread(m_image_taken, Imagemode);

        //check how many images are read in.
        if (m_Ref.empty() || m_Taken.empty()) {
            cout << "Image is empty." << endl;
            stat = false;
        }

        else if (!(m_Ref.empty() && m_Taken.empty())) {
            cout << "Read m_Ref & m_Taken successful." << endl;
            stat = true;
        }
    }

    if (images.size() == 1) {
        m_image_ref = images[0];
        m_Ref = imread(m_image_ref, Imagemode);
        if (m_Ref.empty()) {
            cout << "Image m_Ref is empty." << endl;
            stat = false;
        } else {
            cout << "Read in successful." << endl;
            stat = true;
        }
    }


    return stat;
}

bool TestingInterface::checkImages()
{
    bool stat = true;

    // check if data of both images are the same.

    return stat;
}
