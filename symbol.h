#ifndef SYMBOL_H
#define SYMBOL_H

#include "testinginterface.h"


class Symbol : public TestingInterface
{

private:
    Rect m_Roibase;
//	 Threshold
    double m_th;
    Mat Thresh;
// symbols
    int m_loc_x, m_loc_y, m_w, m_h;


public:
virtual ~Symbol();
       bool readinSymbols(const string image);
       // Find symbol in taken picture (here it is m_ref).
     virtual  bool check(string symbol_wanted, double th, SENSIBILITY sense);

};

#endif // SYMBOL_H
