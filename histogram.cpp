#include "histogram.h"



bool Histogram::calculatehistogram(string image)
{
    bool stat = false;

    Mat src, b_hist, g_hist, r_hist;

    if (m_Ref.empty() && m_Taken.empty()) {
        cout << "Images not read in" << endl;
        stat = false;
    }

    else {

        if (image == "m_Ref") {
            src = m_Ref;
            cout << "m_Ref is being calculated" << endl;
            stat = true;
        }
        if (image == "m_Taken") {
            src = m_Taken;
            cout << "m_Taken is being calculated" << endl;
            stat = true;
        } else if (image != "m_Ref" && image != "m_Taken") {
            cout << "No Images are loaded." << endl;
            stat = false;
        }

        if (stat) {
            Mat hsv_src;

            // Convert BGR to HSV, because its better -> RESEARCH
            cvtColor(src, hsv_src, COLOR_BGR2HSV);

            /// Establish the number of bins, ..
            int h_bins = 50;
            int s_bins = 60;
            int histSize[] = { h_bins, s_bins };
            int channels[] = { 0, 1 };

            float h_ranges[] = { 0, 180 };
            float s_ranges[] = { 0, 256 };
            const float *ranges[] = { h_ranges, s_ranges };

            Mat checktemp;
            Mat Normalized;
            calcHist(&hsv_src, 1, channels, Mat(), checktemp, 2, histSize,
                    ranges, true, false);

            if (checktemp.empty()) {
                cout << "checktemp empty" << endl;
                stat = false;
            }

            normalize(checktemp, Normalized, 0, 1, NORM_MINMAX, -1, Mat());

            if (Normalized.empty()) {
                cout << "Normalized empty" << endl;
                stat = false;
            }

            // copy check to attribute.

            if (image == "m_Ref") {
                Normalized.copyTo(m_Histogram_Ref);
            }
            if (image == "m_Taken") {
                Normalized.copyTo(m_Histogram_Taken);
            }

            stat = true;
        }
    }

    return stat;
}

bool Histogram::check(TestingInterface::SENSIBILITY sense)
{
    bool stat = false;

    if ((!calculatehistogram("m_Ref"))) {
        cout << "Calculate m_Ref not possible" << endl;
        stat = false;
    }

    if (!(calculatehistogram("m_Taken"))) {
        cout << "Calculate m_Taken not possible" << endl;
        stat = false;
    } else {

        char *list[] = { "Correlation", "Chi-Square", "Intersection",
                " Bhattacharyya distance", "Alternative Chi-Square",
                " Kullback-Leibler divergence" };

        /// Apply the check comparison methods
        for (int i = 0; i < 6; i++) {
            int compare_method = i;
            double ref_ref = compareHist(m_Histogram_Ref, m_Histogram_Ref,
                    compare_method);
            double taken_taken = compareHist(m_Histogram_Taken,
                    m_Histogram_Taken, compare_method);
            m_histcompvalue[i] = compareHist(m_Histogram_Ref, m_Histogram_Taken,
                    compare_method);

            printf(
                    " Method: %s	\n Reference, m_Taken, m_Ref-m_Taken: %f, %f, %f \n",
                    list[i], ref_ref, taken_taken, m_histcompvalue[i]);
        }
        // TODO: evaluate reasonable hist-values and Algorithm for limits.
        stat = true;
        switch (sense) {
        case STRONG:
            if (m_histcompvalue[0] > 0.00005)
                stat = false;
            break;
        case MEDIUM:
            if (m_histcompvalue[2] > 0.00001)
                stat = false;
            break;
        case WEAK:
            if (m_histcompvalue[0] < 1)
                stat = false;
            break;
        }

        printf("Done \n");
    }

    return stat;


}
