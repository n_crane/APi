#include "testapi.h"
/**
    Project: TESTAPI

    @author Niclas Crane
    @version 1.1 17/08/17
*/

/**
 * Reads in Images
 * @param Vector of images, Imagereadmode
 * @return Process went correct or not
 */

bool TestValidation::readinImages(std::vector<string> &images,
        cv::ImreadModes Imagemode) {

    bool stat = false;

    if (images.size() > 1) {
        //Read in images by parameter.
        m_image_ref = images[0];
        m_image_taken = images[1];
        m_Ref = imread(m_image_ref, Imagemode);
        m_Taken = imread(m_image_taken, Imagemode);

        //check how many images are read in.
        if (m_Ref.empty() || m_Taken.empty()) {
            cout << "Image is empty." << endl;
            stat = false;
        }

        else if (!(m_Ref.empty() && m_Taken.empty())) {
            cout << "Read m_Ref & m_Taken successful." << endl;
            stat = true;
        }
    }

    if (images.size() == 1) {
        m_image_ref = images[0];
        m_Ref = imread(m_image_ref, Imagemode);
        if (m_Ref.empty()) {
            cout << "Image m_Ref is empty." << endl;
            stat = false;
        } else {
            cout << "Read in successful." << endl;
            stat = true;
        }
    }
    return stat;
}

/**
 * Calculate the check of the images, which are stored in class attributes
 * @param image you want to calculate check of
 * @return Process went correct or not
 */

bool TestValidation::calculatecheck(string image) {

    bool stat = false;

    Mat src, b_hist, g_hist, r_hist;

    if (m_Ref.empty() && m_Taken.empty()) {
        cout << "Images not read in" << endl;
        stat = false;
    }

    else {

        if (image == "m_Ref") {
            src = m_Ref;
            cout << "m_Ref is being calculated" << endl;
            stat = true;
        }
        if (image == "m_Taken") {
            src = m_Taken;
            cout << "m_Taken is being calculated" << endl;
            stat = true;
        } else if (image != "m_Ref" && image != "m_Taken") {
            cout << "No Images are loaded." << endl;
            stat = false;
        }

        if (stat) {
            Mat hsv_src;

            // Convert BGR to HSV, because its better -> RESEARCH
            cvtColor(src, hsv_src, COLOR_BGR2HSV);

            /// Establish the number of bins, ..
            int h_bins = 50;
            int s_bins = 60;
            int histSize[] = { h_bins, s_bins };
            int channels[] = { 0, 1 };

            float h_ranges[] = { 0, 180 };
            float s_ranges[] = { 0, 256 };
            const float *ranges[] = { h_ranges, s_ranges };

            Mat checktemp;
            Mat Normalized;
            calcHist(&hsv_src, 1, channels, Mat(), checktemp, 2, histSize,
                    ranges, true, false);

            if (checktemp.empty()) {
                cout << "checktemp empty" << endl;
                stat = false;
            }

            normalize(checktemp, Normalized, 0, 1, NORM_MINMAX, -1, Mat());

            if (Normalized.empty()) {
                cout << "Normalized empty" << endl;
                stat = false;
            }

            // copy check to attribute.

            if (image == "m_Ref") {
                Normalized.copyTo(m_check_Ref);
            }
            if (image == "m_Taken") {
                Normalized.copyTo(m_check_Taken);
            }

            stat = true;
        }
    }

    return stat;
}
/**
 * Compare the check of the images, which are stored in class attributes
 * @param the sensibility of how detailed images are compared.
 * @return Process went correct or not
 */
bool TestValidation::check(SENSIBILITY sense) {
    bool stat = false;

    if ((!calculatecheck("m_Ref"))) {
        cout << "Calculate m_Ref not possible" << endl;
        stat = false;
    }

    if (!(calculatecheck("m_Taken"))) {
        cout << "Calculate m_Taken not possible" << endl;
        stat = false;
    } else {

        char *list[] = { "Correlation", "Chi-Square", "Intersection",
                " Bhattacharyya distance", "Alternative Chi-Square",
                " Kullback-Leibler divergence" };

        /// Apply the check comparison methods
        for (int i = 0; i < 6; i++) {
            int compare_method = i;
            double ref_ref = compareHist(m_Histogram_Ref, m_Histogram_Ref,
                    compare_method);
            double taken_taken = compareHist(m_check_Taken,
                    m_check_Taken, compare_method);
            m_histcompvalue[i] = compareHist(m_check_Ref, m_check_Taken,
                    compare_method);

            printf(
                    " Method: %s	\n Reference, m_Taken, m_Ref-m_Taken: %f, %f, %f \n",
                    list[i], ref_ref, taken_taken, m_histcompvalue[i]);
        }
        // TODO: evaluate reasonable hist-values and Algorithm for limits.
        stat = true;
        switch (sense) {
        case STRONG:
            if (m_histcompvalue[0] > 0.00005)
                stat = false;
            break;
        case MEDIUM:
            if (m_histcompvalue[2] > 0.00001)
                stat = false;
            break;
        case WEAK:
            if (m_histcompvalue[0] < 1)
                stat = false;
            break;
        }

        printf("Done \n");
    }

    return stat;
}

/**
 * Check if images are of the same size

 * @return Process went correct or not
 */

bool TestValidation::checkImages() {
    bool stat = true;
    //check Size
    if (!(m_Ref.size == m_Taken.size)) {
        cout << "Images are not the of the same size" << endl;
        stat = false;
    }
    cout << "m_Ref Size: " << m_Ref.size << endl;
    cout << "m_Taken Size: " << m_Taken.size << endl;

    // TODO: Further checks?
    return stat;
}

/**
 * Compare checks of ref and taken in specifique region.
 * @param Region of interest,the sensibility of how detailed images are compared.
 * @return Process went correct or not
 */

bool TestValidation::check(m_ROI *regOI, SENSIBILITY sense) {
    bool stat = false;

    //Check Infos about images

    if (!(checkImages())) {
        cout << "Basic Stats about images are not the same" << endl;
        stat = false;
    }

    else {

        m_Roibase = Rect(regOI->x, regOI->y, regOI->h, regOI->w);
        Mat mask = m_Taken(m_Roibase);
        stat = check(sense);

    }
    return stat;
}


/**
 * Compare checks of ref and taken in specifique region and apply threshold before.
 * @param Region of interest,threshold to apply before,the sensibility of how detailed images are compared.
 * @return Process went correct or not
 */


bool TestValidation::check(m_ROI *regOI, double th, SENSIBILITY sense) {
    bool stat = false;
    int const max_BINARY_value = 255;

    threshold(m_Taken, Thresh, th, max_BINARY_value, 3);
    Thresh.copyTo(m_Taken);
    stat = check(regOI, sense);

    return stat;
}

/**
 * Read in images for Reference or Taken or both
 * @param image of symbol to search in image
 * @return Process went correct or not
 */

bool TestValidation::readinSymbols(const string image) {

    m_Taken = imread(image, 0);
    imshow("Gray", m_Taken);
    waitKey(0);
//	cvtColor(m_Taken, m_Taken, COLOR_BGR2GRAY);
//	imshow("Graygray", m_Taken);
//	waitKey(0);
    Mat Threshold;
    m_th = 123;
    threshold(m_Taken, Threshold, m_th, 255, 3);
    Threshold.copyTo(Threshold);
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    findContours(m_Taken, contours, hierarchy, 0, 1);
    cout << "Numbers of contours:" << contours.size() << endl;
    imshow("contours", m_Taken);
    waitKey(0);
// find contours, analyse contours, store each in one file.

    return true;
}


/**
 * // Find symbol in taken picture (here it is m_ref).
 * @param image of symbol to search in image
 * @return Process went correct or not
 */

bool TestValidation::symbol(string symbol_wanted, double th,
        SENSIBILITY sense) {

    // read in symbol-file , apply threshold and use template matchin
    Mat symbol, ref_gr, result;
    bool stat = false;
    //TODO: maybe the template has to be reversed:
    //w, h = template.shape[::-1]

    // start Python program with parameter, by running script and pass parameters.
    // The script will execute "TemplateMatching_Python.py" which will try to find
    // the dedicated symbol in the base image and writes its position into a text-
    // file to pass the information.

    string pyth = "./script.sh";
    ostringstream osref;
    osref << m_image_ref;
    string strref = osref.str();
    ostringstream ostaken;
    ostaken << symbol_wanted;
    string strtaken = ostaken.str();

    // Execute python-script
    pyth += " ";
    pyth += strref;
    pyth += " ";
    pyth += strtaken;
    cout << pyth << endl;
    std::system(pyth.c_str());

    // Read in Parametersof python-script.

    ifstream File;
    string line;
    File.open("Template.txt");
    getline(File, line);
    unsigned int separator_pos = 0, start_pos = 0;
    char separator = ',';
    string fields[10];
    int count = 0;

    getline(File, line);
    while (separator_pos != string::npos) {
        if (count >= 7)
            break;
        if (separator_pos == 0) {
            separator_pos = line.find_first_of(",");
            if (separator_pos != string::npos) {
                separator = line[separator_pos];
            }

        } else {
            separator_pos = line.find(separator, start_pos);
        }

        if (separator_pos != string::npos) {
            fields[count] = line.substr(start_pos, separator_pos - start_pos);
            start_pos = separator_pos + 1;
            cout << "FIELDS:" << fields[count] << endl;
            count = count + 1;

        }
    }
    remove("Template.txt");

    // evaluate Data, Get location of template, if found.

    if (fields[0] == "No matches are found")
        stat = false;
    else {
        string::size_type sz;
        double Threshold_symbol;
        stringstream convert(fields[1]);
        convert >> Threshold_symbol;
        stringstream convert1(fields[3]);
        convert1 >> m_loc_x;
        stringstream convert2(fields[4]);
        convert2 >> m_loc_y;
        stringstream convert3(fields[5]);
        convert3 >> m_w;
        stringstream convert4(fields[6]);
        convert4 >> m_h;
        stat = true;
    }

    return stat;
}

bool TestValidation::readinSweep(const string image, bool reftak) {



}

bool TestValidation::getlocationSweep(const string image) {
}

//Acquisition
bool TestValidation::sweepangle(string tipneedle, m_ROI needle_source) {
    bool stat = true;
    // flag true means acquisition, wrong means validation
    // 1read in first image
    // 2read in tip of needle (Validation)
    // 3find tip with template.
    // 4calculate angle.
    // 5store angle (Acquisition)
    // 6compare to reference (validation).
    // 7read in next image. start at 3.

    for (int i = 0; i < sizeof(m_Sweeparray_ref); i++) {

        m_Sweeparray_ref[i].copyTo(m_Ref);

        if (!(symbol(tipneedle, 0, STRONG))) {
            stat = false;
            break;
        } else {
            // calculate angle between:
            // tip of needle
            // source of needle
            // and lot of source
            m_angles[i] = getAngleABC(m_loc_x, m_loc_y, needle_source.x,
                    needle_source.y, needle_source.x, needle_source.y + 50);
            cout<<m_angles[i]<<endl;
        }


    }

    return stat;

}

double TestValidation::getAngleABC(int ax, int ay, int bx, int by, int cx,
        int cy) {

    int abx = bx - ax;
    int aby = by - ay;
    int acx = bx - cx;
    int acy = by - cy;

    float dotabac = (abx * aby + acx * acy);
    float lenab = sqrt(abx * abx + aby * aby);
    float lenac = sqrt(acx * acx + acy * acy);

    float dacos = dotabac / lenab / lenac;

    float rslt = acos(dacos);
    float rs = (rslt * 180) / 3.141592;

    return (double) rs;

}



bool TestValidation::sweepangle(string tipneedle, double* angle,
        m_ROI needle_source) {

    bool stat = true;
    double error = 0;

    for (int i = 0; i < sizeof(m_Sweeparray_taken); i++) {

        m_Sweeparray_ref[i].copyTo(m_Ref);

        if (!(symbol(tipneedle, 0, STRONG))) {
            stat = false;
            cout << "No needle found" << endl;
            break;
        } else {
            // calculate angle between:
            // tip of needle
            // source of needle
            // and lot of source
            m_angles[i] = getAngleABC(m_loc_x, m_loc_y, needle_source.x,
                    needle_source.y, needle_source.x, needle_source.y + 50);

            //TODO: Find reasonalbe value for error
            error = abs(m_angles[i] - angle[i]);
            if (error > 0.5) {
                cout << "needle out of range" << endl;
                stat = false;
            }
        }

    }

    return stat;
}
